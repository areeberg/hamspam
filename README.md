# Teste Prático PSafe

## Enunciado
Criar um projeto em python para classificação de texto entre  **ham**  e  **spam**, do dataset [https://archive.ics.uci.edu/ml/machine-learning-databases/00228/](https://archive.ics.uci.edu/ml/machine-learning-databases/00228/)

O projeto deve conter a etapa de  **cross validation**,  **extração de features**, salvar um modelo pronto e uma forma de aplicar classificação em novas entradas carregando um modelo previamente salvo.  

A métrica utilizada para validação fica a sua escolha, mas essa escolha precisa ser justificada.

Conforme conversamos, a data de entrega é ate dia  **02/09**.


# Soluções

Foram desenvolvidas duas soluções para a teste prático
- Método 1: utilizar handcraft features e classificador seguindo o raciocínio da publicação original
- Método 2: utilizar método SOTA (Distilbert) para tokenização e classificação

## O dataset

O dataset é baseado no artigo "Almeida, Tiago A., José María G. Hidalgo, and Akebo Yamakami. "Contributions to the study of SMS spam filtering: new collection and results." _Proceedings of the 11th ACM symposium on Document engineering_. 2011." (www.dt.fee.unicamp.br/~tiago/smsspamcollection/doceng11.pdf).

Análise estatística: 
- Total de 4827 (86.6%) SMS e 747 (13.4%) spam.
- Existem 4 fontes de dados diferentes
- Os dados são desbalanceados
- Como não há timestamps, não há como verificar a variação da distribuição dos dados ao longo do tempo

## Solução 1 - handcraft features + XGBoost

Para esta versão utilizei o método TF_IDF para extração de features, pois o comprimento das mensagens de ham e spam são na sua maioria diferentes (como apresentado no arquivo .ipynb).

TF-IDF - term frequency-inverse document frequency

TF(t) = (Number of times term t appears in a document) / (Total number of terms in the document).

DF(t) = log_e(Total number of documents / Number of documents with term t in it).

- Os dados foram separados em treino e teste apenas, pois é necessário usar validação cruzada. 
- O método de validação cruzada utilizada é estratificada pois os dados são desbalanceados.

Como foi utilizado o XGBoost, que possui boa capacidade de generalização em relação ao ajuste de hiperpâmetros (comparados a métodos kernel, como apresentado pelo paper), o resultado da validação cruzada não é estatisticamente signifcativo:
Best: 0.958023 using {'scale_pos_weight': 15} 0.952801 (0.016829) 
with: {'scale_pos_weight': 1} 0.953403 (0.018214) 
with: {'scale_pos_weight': 3} 0.954635 (0.017108) 
with: {'scale_pos_weight': 5} 0.955775 (0.016712) 
with: {'scale_pos_weight': 6} 0.955350 (0.017167) 
with: {'scale_pos_weight': 7} 0.955217 (0.016700) 
with: {'scale_pos_weight': 9} 0.956144 (0.016466)
 with: {'scale_pos_weight': 10} 0.958023 (0.015852) 
 with: {'scale_pos_weight': 15}

- Para a análise de resultado foram escolhidas algumas métricas apresentadas do paper.
- Métricas - TN, TP, FP, FN, Matthews Correlation e Accuracy
- FP - Gera alerta (bloqueia mensagem), logo é necessário reenviar as mensagens (ham).
- FN - Não gera alerta (não bloqueia mensagem que deveria ser bloqueado). Caso crítico.

### Resultados:
- Accuracy=0.9677
- TP=129
- TN=950
- FP=16
- FN=20
- Matthews Correlation=0.859

Como não há teste de significância no paper (a nossa referência), os valores apresentados estão dentro do previsto. Para evoluir o sistema, melhor do que escolher métodos ou ajustar hiperparâmetros é coletar mais dados.

## Solução 2 - Distilbert

Para esta solução foi utilizado um método baseado no SOTA, o DistilBERT, a distilled version of BERT: smaller, faster, cheaper and lighter (Sanh, Victor, et al. "DistilBERT, a distilled version of BERT: smaller, faster, cheaper and lighter." arXiv preprint arXiv:1910.01108 (2019).), pois é um Transformer baseado no BERT e é menor e mais leve para viabilizar o treinamento de modelos em computadores "mais modestos".

Utilizamos dois modelos prétreinados do DistilBERT da Huggingface, um para o tokenizer e outro para a rede BERT. 
Para o tokenizer foi considerado o case sem case-sensitive, e para o processo de token o transferlearning de um modelo pré-treinado.
Para a inferência, utilizamos outro modelo pré-treinado (freeze nas primeiras camadas), e apenas uma rede neural simples na saída para realizar o fine-tuning. 

Foi realizado o processo de validação cruzada estratificada, com a observação que existe uma chance de overfitting devido ao baixo volume de dados. Contudo, conseguimos observar a convergência do erro do modelo (apresentado no código).

utilizei as mesmas métricas para comparação, e como era de se esperar os resultados são superiores ao paper e ao outro método proposto.

### Resultados:
- Accuracy=0.988
- TP=104
- TN=998
- FP=5
- FN=8
- Matthews Correlation=0.934

### Paper benchmark - linear SVM
- Accuracy= 97.64 
- Matthews Correlation=0.893
